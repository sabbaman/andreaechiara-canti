var h3_base = 1.8;
var p_base = 1.2;
$(document).ready(function(){
    var cont = $(".container");
    var index =$(".index-cont");
    for(var i = 0; i< lista_canti.length;i++){
        var main_div = $("<div></div>");
        var title = $("<h3></h3>");
        var testo = $("<div></div>");
        title.html(lista_canti[i][0]);
        testo.html("<p>"+lista_canti[i][1].replace(/\n\n/g,'</p><p>').replace(/\n/g,'<br />')+"</p>");
        main_div.append("<a name='song"+i.toString()+"'></a>").append(title).append(testo);
        cont.append(main_div);
        
        var voce_index = $("<li><a href='#"+"song"+i.toString()+"'>"+lista_canti[i][0]+"</a></li>");
        index.append(voce_index);
    }

    $(".zoomup").click(function() {
        h3_base = h3_base+0.1;
        p_base = p_base+0.1;
        $("h3").css("font-size",h3_base.toString()+"em");
        $("p").css("font-size",p_base.toString()+"em");
    });

    $(".zoomdown").click(function() {
        h3_base = h3_base-0.1;
        p_base = p_base-0.1;
        if(h3_base<1.8) h3_base = 1.8;
        if(p_base<1.2) p_base = 1.2;
        $("h3").css("font-size",h3_base.toString()+"em");
        $("p").css("font-size",p_base.toString()+"em");
    });
    
});

var lista_canti = [
    [
        "Tanto pe&#x27; cant&#xE0;",
`Pe&#x27; fa la vita meno amara
me so comprato &#x27;sta chitara,
e quann&#x27;er sole scenne e more
me sento &#x27;n c&#xF2;re cantatore.
La voce &#xE8; poca ma &#x27;ntonata,
nun serve a f&#xE0; la serenata,
ma solamente a f&#xE0; in magnera
de famme un sogno a prima sera.

Tanto pe&#x27; cant&#xE0;,
perch&#xE9; me sento &#x27;n friccico ner c&#xF2;re,
tanto pe&#x27; sogn&#xE0;,
perch&#xE9; ner petto me ce naschi un fiore.
fiore de lill&#xE0;
che m&#x27;ariporti verso er primo amore,
che sospiravi le canzone mie,
e m&#x27;arintontonivi de bug&#xEC;e.

Canzoni belle e appassionate
che Roma mia m&#x27;ha ricordate,
cantate solo pe&#x27; dispetto,
ma co &#x27;na smania drent&#x27;ar petto;
io nun ve canto a voce piena,
ma tutta l&#x27;anima &#xE8; serena;
e quanno er c&#xE8;lo se scolora
de me nessuna se &#x27;nnamora.

Tanto pe&#x27; cant&#xE0;,
perch&#xE9; me sento &#x27;n friccico ner c&#xF2;re,
tanto pe&#x27; sogn&#xE0;,
perch&#xE9; ner petto me ce naschi un fiore.
fiore de lill&#xE0;
che m&#x27;ariporti verso er primo amore,
che sospiravi le canzone mie,
e m&#x27;arintontonivi de bug&#xEC;e.`
],
[
    "Un&#x27;estate italiana",
`Forse non sar&#xE0; una canzone
A cambiare le regole del gioco
Ma voglio viverla cosi quest&#x27;avventura
Senza frontiere e con il cuore in gola

E il mondo in una giostra di colori
E il vento accarezza le bandiere
Arriva un brivido e ti trascina via
E sciogli in un abbraccio la follia

Notti magiche
Inseguendo un goal
Sotto il cielo
Di un&#x27;estate italiana

E negli occhi tuoi
Voglia di vincere
Un&#x27;estate
Un&#x27;avventura in pi&#xF9;

Quel sogno che comincia da bambino
E che ti porta sempre pi&#xF9; lontano
Non &#xE8; una favola, e dagli spogliatoi
Escono i ragazzi e siamo noi

Notti magiche
Inseguendo un goal
Sotto il cielo
Di un&#x27;estate italiana

E negli occhi tuoi
Voglia di vincere
Un&#x27;estate
Un&#x27;avventura in pi&#xF9;

Notti magiche
Inseguendo un goal
Sotto il cielo
Di un&#x27;estate italiana

E negli occhi tuoi
Voglia di vincere
Un&#x27;estate
Un&#x27;avventura in pi&#xF9;
`
],
[
    "Un&#x27;Avventura",
`Non sar&#xE0;
Un&#x27;avventura
Non pu&#xF2; essere soltanto una primavera
Questo amore
Non &#xE8; una stella
Che al mattino se ne va,
Oh no no no no no no.

Non sar&#xE0;
Un&#x27;avventura
Questo amore &#xE8; fatto solo di poesia
Tu sei mia,
Tu sei mia
Fino a quando gli occhi miei
Avran luce per guardare gli occhi tuoi.

Innamorato
Sempre di pi&#xF9;
In fondo all&#x27;anima
Per sempre tu.
Perch&#xE9; non &#xE8; una promessa
Ma &#xE8; quel che sar&#xE0;
Domani e sempre
Sempre vivr&#xE0;,
Sempre vivr&#xE0;,
Sempre vivr&#xE0;,
Sempre vivr&#xE0;.

No, non sar&#xE0;
Un&#x27;avventura, un&#x27;avventura
Non &#xE8; un fuoco che col vento pu&#xF2; morire
Ma vivr&#xE0;
Quanto il mondo
Fino a quando gli occhi miei
Avran luce per guardare gli occhi tuoi.

Innamorato
Sempre di pi&#xF9;
In fondo all&#x27;anima
Per sempre tu.
Perch&#xE9; non &#xE8; una promessa
Ma &#xE8; quel che sar&#xE0;
Domani e sempre
Sempre vivr&#xE0;,
Perch&#xE9; io sono innamorato
E sempre di pi&#xF9;
In fondo all&#x27;anima
Ci sei per sempre tu...
`],
[
    "Carramba",
`E lass&#xF9; sul Monte Nero
c&#x27;&#xE8; una piccola taverna
ci son dodici briganti
al chiaror di una lanterna.

Caramba beviamo del whisky
caramba beviamo del gin
e tu non dar retta al cuore
che tutto passer&#xE0;.

Mentre tutti son festanti
uno solo resta muto
ha il bicchiere ancora pieno
come mai non ha bevuto?

Caramba beviamo del whisky
caramba beviamo del gin
e tu non dar retta al cuore
che tutto passer&#xE0;.

Ma non pu&#xF2; dimenticare
il brigante la sua bella
i suoi occhi luminosi
la sua bocca tanto bella.
`],
[
    "La Muralla",
`Para hacer esta muralla,
tr&#xE1;iganme todas las manos
los negros, sus manos negras
los blancos, sus blancas manos.

Una muralla que vaya
desde la playa hasta el monte
desde el monte hasta la playa,
all&#xE1; sobre el horizonte.

&#x2014;&#xA1;Tun, tun!
&#x2014;&#xBF;Qui&#xE9;n es?
&#x2014;Una rosa why un clavel...
&#x2014;&#xA1;Abre la muralla!
&#x2014;&#xA1;Tun, tun!
&#x2014;&#xBF;Qui&#xE9;n es?
&#x2014;El sable del coronel...
&#x2014;&#xA1;Cierra la muralla!
&#x2014;&#xA1;Tun, tun!
&#x2014;&#xBF;Qui&#xE9;n es?
&#x2014;La paloma why el laurel...
&#x2014;&#xA1;Abre la muralla!
&#x2014;&#xA1;Tun, tun!
&#x2014;&#xBF;Qui&#xE9;n es?
&#x2014;El gusano why el ciempi&#xE9;s...
&#x2014;&#xA1;Cierra la muralla!

Al coraz&#xF3;n del amigo:
abre la muralla;
al veneno why al pu&#xF1;al:
cierra la muralla;
al mirto why la yerbabuena:
abre la muralla;
al diente de la serpiente:
cierra la muralla;
al coraz&#xF3;n del amigo:
abre la muralla;
al ruise&#xF1;or en la flor&#x2026;

Alcemos esta muralla
juntando todas las manos;
los negros, sus manos negras
los blancos, sus blancas manos.

Una muralla que vaya
desde la playa hasta el monte
desde el monte hasta la playa,
all&#xE1; sobre el horizonte.
`],
["Quand sona i campan",
`Quand mi t&#x27;hoo vista pass&#xE0;
un d&#xEC; per la mia via
me sun sent&#xEC; &#x27;nnamur&#xE0;a
mia piccula Maria
quand poeu te m&#x27;ee dumand&#xE0;a
se mi te spusar&#xEC;a
me sun sent&#xEC; tutt&#x27;el sangh a buj
e t&#x27;hou respost de si

Quand sona i campann, din don, din dan,
a la periferia
quand sona i campann, din don, din dan,
mi te speti, o Maria
per d&#xEC; che fra&#x27;n an, din don, din dan,
te sar&#xE8;t tuta mia
vedi gi&#xE0; du stansett coun tr&#xEC; o quatter masscett che me ciamen pap&#xE0;
sun cuntent cume on ratt, mi doo foera de matt per la felicit&#xE0;.

Parli coun tuti de ti
la sera e la mattina
e speti semper quel d&#xEC;
de fatt la mia spusina
mi sun le alegher mem&#xEC;
in tuta l&#x27;ufficina
canti, lavuri, me senti seren
perch&#xE8; te voeri ben

Quand sona i campann, din don, din dan,
a la periferia
quand sona i campann, din don, din dan,
mi te speti, o Maria
per d&#xEC; che fra&#x27;n an, din don, din dan,
te sar&#xE8;t tuta mia
vedi gi&#xE0; du stansett coun tr&#xEC; o quatter masscett che me ciamen pap&#xE0;
sun cuntent cume on ratt, mi doo foera de matt per la felicit&#xE0;`
],
["Cielito Lindo",`

De la sierra, morena
Cielito lindo, vienen bajando
Un par de ojitos negros, cielito lindo
De contrabando 

De la sierra, morena
Cielito lindo, vienen bajando
Un par de ojitos negros, cielito lindo
De contrabando 

Ay, ay, ay, ay
Canta y no llores
Porque cantando se alegran
Cielito lindo, los corazones

Ese lunar que tienes
Cielito lindo, junto a la boca
No se lo des a nadie, cielito lindo
Que a m&#xED; me toca

Ese lunar que tienes
Cielito lindo, junto a la boca
No se lo des a nadie, cielito lindo
Que a m&#xED; me toca

Ay, ay, ay, ay
Canta y no llores
Porque cantando se alegran
Cielito lindo, los corazones`],
["O&#x27; surdato &#x27;nnamurato",
`Staje luntana da stu core,
a te volo cu &#x27;o penziero:
niente voglio e niente spero
ca tenerte sempe a fianco a me!
Si&#x27; sicura &#x27;e chist&#x27;ammore
comm&#x27;i&#x27; s&#xF3;&#x27; sicuro &#x27;e te...

Oje vita, oje vita mia...
oje core &#x27;e chistu core...
si&#x27; stata &#x27;o primmo ammore...
e &#x27;o primmo e ll&#x27;&#xF9;rdemo sarraje pe&#x27; me!

Quanta notte nun te veco,
nun te sento &#x27;int&#x27;a sti bbracce,
nun te vaso chesta faccia,
nun t&#x27;astregno forte &#x27;mbraccio a me?!
Ma, scet&#xE1;nnome &#x27;a sti suonne,
mme faje chiagnere pe&#x27; te...

Oje vita, oje vita mia...
oje core &#x27;e chistu core...
si&#x27; stata &#x27;o primmo ammore...
e &#x27;o primmo e ll&#x27;&#xF9;rdemo sarraje pe&#x27; me!

Scrive sempe e sta&#x27; cuntenta:
io nun penzo che a te sola...
Nu penziero mme cunzola,
ca tu pienze sulamente a me...
&#x27;A cchi&#xF9; bella &#x27;e tutt&#x27;&#x27;e bbelle,
nun &#xE8; maje cchi&#xF9; bella &#x27;e te!

Oje vita, oje vita mia...
oje core &#x27;e chistu core...
si&#x27; stata &#x27;o primmo ammore...
e &#x27;o primmo e ll&#x27;&#xF9;rdemo sarraje pe&#x27; me!`],
["Io ho in mente te",
`Apro gli occhi e ti penso
Ed ho in mente te
Ed ho in mente te

Io cammino per le strade
Ed ho in mente te
Ed ho in mente te

Ogni mattina uo, uo
Ed ogni sera, uo, uo
Ed ogni notte, te

Io lavoro pi&#xF9; forte
Ma ho in mente te
Ma ho in mente te

Ogni mattina uo, uo
Ed ogni sera, uo, uo
Ed ogni notte, te

Che cos&#x27;ho nella testa
Che cos&#x27;ho nelle scarpe
No, non so cos&#x27;&#xE8;
Ho voglia di andare, uo, uo
Di andarmene via, uo, uo
Non voglio pensar ma poi ti penso

Apro gli occhi e ti penso
Ed ho in mente te
Ed ho in mente te

Ogni mattina, uo, uo
Ed ogni sera, uo, uo
Ed ogni notte te`],
["Mi sei scoppiato dentro al cuore",`
Era solamente ieri sera
io parlavo con gli amici
scherzavamo fra di noi
e tu, e tu, e tu
tu sei arrivato
mi hai guardato e allora
tutto &#xE8; cambiato per me.

Mi sei scoppiato dentro il cuore
all&#x27;improvviso, all&#x27;improvviso
non so perch&#xE8;, non lo so perch&#xE8;
all&#x27;improvviso, all&#x27;improvviso
sar&#xE0; perch&#xE8; mi hai guardato
come nessuno mi ha guardato mai
mi sento viva all&#x27;improvviso per te.

Ora io non ho capito ancora
non so come pu&#xF2; finire
quello che succeder&#xE0;
ma tu, ma tu, ma tu
tu l&#x27;hai capito
l&#x27;hai capito e ho visto 
che eri cambiato anche tu.

Mi sei scoppiato dentro il cuore
all&#x27;improvviso, all&#x27;improvviso
non so perch&#xE8;, non lo so perch&#xE8;
all&#x27;improvviso, all&#x27;improvviso
sar&#xE0; perch&#xE8; mi hai guardato
come nessuno mi ha guardato mai
mi sento viva all&#x27;improvviso per te.`],
["Timoneiro",
`N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
&#xC9; ele quem me carrega
Como nem fosse levar
&#xC9; ele quem me carrega
Como nem fosse levar

E quanto mais remo mais rezo
Pra nunca mais se acabar
Essa viagem que faz
O mar em torno do mar
Meu velho um dia falou
Com seu jeito de avisar
Olha, o mar n&#xE3;o tem cabelos
Que a gente possa agarrar

N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
&#xC9; ele quem me carrega
Como nem fosse levar
&#xC9; ele quem me carrega
Como nem fosse levar

Timoneiro nunca fui
Que eu n&#xE3;o sou de velejar
O leme da minha vida
Deus &#xE9; quem faz governar
E quando algu&#xE9;m me pergunta
Como se faz pra nadar
Explico que eu n&#xE3;o navego
Quem me navega &#xE9; o mar

N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
&#xC9; ele quem me carrega
Como nem fosse levar
&#xC9; ele quem me carrega
Como nem fosse levar

A rede do meu destino
Parece a de um pescador
Quando retorna vazia
Vem carregada de dor
Vivo num redemoinho
Deus bem sabe o que ele faz
A onda que me carrega
Ela mesma &#xE9; quem me traz

N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
N&#xE3;o sou eu quem me navega
Quem me navega &#xE9; o mar
&#xC9; ele quem me carrega
Como nem fosse levar
&#xC9; ele quem me carrega
Como nem fosse levar`],
["Guantanamera",
`Yo soy un hombre sincero
De donde crece la palma(x2)
Y antes de morir me quiero
Echar mis versos del alma

Guantanamera
Guajira Guantanamera
Guantanamera
Guajira Guantanamera

Cultivo una rosa blanca
En julio como enero (x2)
Para el amigo sincero
Que me da su mano franca

Guantanamera...

Mi verso es de un verde claro
Y de un carm&#xED;n encendido(2x)
Mi verso es un ciervo herido
Que busca en el monte amparo

Guantanamera...`],
["Porompompero",
`El trigo entre todas las flores
Ha elegido a la amapola
Y yo elijo a mi Dolores
Dolores, Lolita, Lola
Y yo, y yo elijo a mi Dolores
Que es la, es la flor m&#xE1;s perfumada
Dol&#xF3;, Dolores, Lolita, Lola

Porompom p&#xF3;n, poropo, porompom pero, per&#xF3;
Poropo, porom pompero, per&#xF3;
Poropo, porompom pon

A los chicos de mi cara
Les voy a poner un candao
Por no ver las cosas raras
De ese ni&#xF1;ato chalao
Por no, por no ver las cosas raras de es&#xE9;
De ese ni&#xF1;ato chalao que te
Que te apunta y no dispara

Porompom p&#xF3;n...

El c&#xE1;teto de tu hermano
Que no me venga con leyes
Que es payo y yo soy gitano
Que llevo sangre de reyes
Que es pa, que es payo y yo soy gitano, que lle
Que llevo sangre de reyes en la
En la palma de la mano

Porompom p&#xF3;n...

Verde era la hoja
Verde era la parra
Debajo del puente
Retumbaba el agua, retumba, retumba, retuuuuum

Porompom p&#xF3;n...`],[
"Piel Canela",
`Que se quede el infinito sin estrellas
O que pierda el ancho mar su inmensidad
Pero el negro de tus ojos que no muera
Y el canela de tu piel se quede igual

Si perdiera el arco iris su belleza
Y las flores su perfume y su color
No seria tan inmensa mi tristeza
Como aquella de quedarme sin tu amor

Me importas tu,y tu, y tu
Y solamente tu,y tu, y tu
Me importas tu,y tu, y tu
y nadie mas que tu
Ojos negros piel canela
Que me llegan a desesperar`
],[
"Haja o que houver",
`Haja o que houver
Eu estou aqui
Haja o que houver
Espero por ti
Volta no vento
O meu amor
Volta depressa
Por favor
Ha quanto tempo
Ja esqueci
Porque fiquei
Longe de ti
Cada momento
E pior
Volta no vento
Por favor

Eu sei, eu sei
Quem es para mim
Haja o que houver
Espero por ti`
],["Ring of Fire",
`Love is a burning thing
And it makes a firery ring
Bound by wild desire
I fell in to a ring of fire

I fell into a burning ring of fire
I went down, down, down
And the flames went higher
And it burns, burns, burns
The ring of fire, the ring of fire (x2)

The taste of love is sweet
When hearts like ours meet
I fell for you like a child
Oh, but the fire went wild

I fell into a burning ring of fire
I went down, down, down
And the flames went higher
And it burns, burns, burns
The ring of fire, the ring of fire (x2)`
],[
"Hello, Mary Lou",
`I say hello Mary Lou, goodbye heart
Sweet Mary Lou I&#x27;m so in love with you
I knew Mary Lou we&#x27;d never part
So hello Mary Lou, goodbye heart

You passed me by one sunny day
Flashed those big brown eyes my way
And ooh I wanted you forever more
Now I&#x27;m not one that gets around
I swear my feet stuck to the ground
And though I never did meet you before

I said hello Mary Lou, goodbye heart
Sweet Mary Lou I&#x27;m so in love with you
I knew Mary Lou we&#x27;d never part
So hello Mary Lou, goodbye heart

I saw your lips I heard your voice
Believe me I just had no choice
Wild horses couldn&#x27;t make me stay away
I thought about a moonlit night
Arms around you, good an&#x27; tight
All I had to see for me to say

I said hello Mary Lou, goodbye heart
Sweet Mary Lou I&#x27;m so in love with you
I knew Mary Lou we&#x27;d never part
So hello Mary Lou, goodbye heart`
],
["Star of the County Down",
`Near Banbridge town in the County Down
one morning last July,
from a boreen green came a sweet colleen
and she smiled as she passed me by.
She looked so sweet from her two bare feet
to the sheen of her nut-brown hair.
Such a coaxing elf, sure I shook myself
for so see I was really there.

From Bantry Bay up to Derry Quay
and from Galway to Dublin town,
no maid I&#x27;ve seen like the brown colleen
that I met in the County Down

As she onward sped, sure I scratched my head
and I looked with a feeling rare.
And I say, say&#x27;s I, to a passer - by,
"Who&#x27;s the maid with the nut-brown hair"?
He smiled at me and he say&#x27;s, say&#x27;s he,
"That&#x27;s the gem of Ireland&#x27;s crown.
She&#x27;s Young Rosie McCann, from the banks of the Bann,
she&#x27;s the star of the County Down"

From Bantry Bay up to Derry Quay
and from Galway to Dublin town,
no maid I&#x27;ve seen like the brown colleen
that I met in the County Down

At the Harvest Fair she&#x27;ll be surely there
and I&#x27;ll dress in my Sunday clothes,
with my shoes shone bright and my hat cocked right
for a smile from my nut-brown rose.
No pipe I&#x27;ll smoke, no horse I&#x27;ll yoke
Till my plough it is rust - coloured brown.
Till a smiling bride, by my own fireside
sits the Star of the County Down

From Bantry Bay up to Derry Quay
and from Galway to Dublin town,
no maid I&#x27;ve seen like the brown colleen
that I met in the County Down`],[
    "Only our Rivers",
`When apples still grow in November,
When Blossoms still bloom from each tree,
When leaves are still green in December,
It&#x27;s then that our land will be free,
I wander her hills and her valleys,
And still through my sorrow I see,
A land that has never known freedom,
And only her rivers run free

I drink to the death of her manhood,
Those men who&#x27;d rather they died,
Than to live in the cold chains of bondage,
When to bring back their rights were denied,
Oh where are you now when we need you,
What burns where the flame used to be,
Are you gone like the snows of last winter,
And will only our rivers run free?

How sweet is life but we&#x27;re crying,
How mellow the wine but it&#x27;s dry,
How fragrant the rose but it&#x27;s dying,
How gentle the breeze but it sighs,
What good is in youth when it&#x27;s aging,
What joy is in eyes that can&#x27;t see,
When there&#x27;s sorrow in sunshine and flowers,
And still only our rivers run free`
],[
"Rose of Allendale",
`The moon was bright, the night was clear
No breeze came over the sea
When mary left her highland home
And wandered forth with me
The flowers be-decked the mountainside
And fragrance filled the vale
But by far the sweetest flower there
Was the rose of allendale

Sweet rose of Allendale
Sweet rose of Allendale
By far the sweetest flower there
Was the rose of Allendale

Where e&#x27;er I wandered east or west
Though fate began to lour
A solace still was she to me
In sorrow&#x27;s lonely hour
When tempests lashed our lonely barque
And rent her quivering sail
One maiden&#x27;s form withstood the storm
&#x27;twas the rose of allendale

Sweet rose of allendale
Sweet rose of allendale
By far the sweetest flower there
Was the rose of Allendale

And when my fever&#x27;d lips were parched
On afric&#x27;s burning sands
She whispered hopes of happiness
And tales of distant lands
My life has been a wilderness
Unblessed by fortune&#x27;s wheel
Had fate not linked my love to hers
The rose of allendale

Sweet rose of allendale
Sweet rose of allendale
By far the sweetest flower there
Was the rose of Allendale`
],[
"The Fields of Athenry",
`But a lonely prison wall,
I heard a young girl calling
Michael they have taken you away,
For you stole trevelyn&#x27;s corn
So the young might see the morn,
Now a prison ship lies waiting in the bay

Low lie, The Fields Of Athenry
Where once we watched the small free birds fly
Our love was on the wing
We had dreams and songs to sing,
Its so lonely round the Fields of Athenry

By a lonely prison wall
I heard a young man calling
&#x27;Nothing matters Mary, when you&#x27;re free&#x27;
Against the famine and the crown,
I rebelled, they brought me down
Now its lonely round the Fields of Athenry

Low lie, The Fields Of Athenry
Where once we watched the small free birds fly
Our love was on the wing
We had dreams and songs to sing,
Its so lonely round the Fields of Athenry

By a lonely harbour wall
She watched the last star falling
As the prison ship sailed out against the sky
Sure she&#x27;ll live in hope and pray
For her love in Botney Bay
Its so lonely round the Fields Of Athenry

Low lie, The Fields Of Athenry
Where once we watched the small free birds fly
Our love was on the wing
We had dreams and songs to sing,
Its so lonely round the Fields of Athenry`
],
["Attenti al lupo",
`C&#x27;&#xE8; una casetta piccola cos&#xEC;
Con tante finestrelle colorate
E una donnina piccola cos&#xEC;
Con due occhi grandi per guardare

E c&#x27;&#xE8; un omino piccolo cos&#xEC;
Che torna sempre tardi da lavorare
E ha un cappello piccolo cos&#xEC;
Con dentro un sogno da realizzare
E pi&#xF9; ci pensa, pi&#xF9; non sa aspettare

Amore mio non devi stare in pena
Questa vita &#xE8; una catena
Qualche volta f&#xE0; un po&#x27; male
Guarda come son tranquilla io
Anche se attraverso il bosco
Con l&#x27;aiuto del buon dio
Stando sempre attenta al lupo
Attenti al lupo
Attenti al lupo
Living together
Living together

Laggi&#xF9; c&#x27;&#xE8; un prato piccolo cos&#xEC;
Con un gran rumore di cicale
E un profumo dolce e piccolo cos&#xEC;
Amore mio &#xE8; arrivata l&#x27;estate
Amore mio &#xE8; arrivata l&#x27;estate

E noi due qui distesi a far l&#x27;amore
In mezzo a questo mare di cicale
Questo amore piccolo cos&#xEC;
Ma tanto grande che mi sembra di volare
E pi&#xF9; ci penso pi&#xF9; non so aspettare

Amore mio non devi stare in pena
Questa vita &#xE8; una catena
Qualche volta fa un po&#x27; male
Guarda come son tranquilla io
Anche se attraverso il bosco
Con l&#x27;aiuto del buon dio
Stando sempre attenta al lupo
Attenti al lupo
Attenti al lupo
Living together
Living together`],
["Tu non mi basti mai",
`Vorrei essere il vestito che porterai
Il rossetto che userai
Vorrei sognarti come non ti ho sognato mai
Ti incontro per strada e divento triste
Perch&#xE9; poi penso che te ne andrai

Vorrei essere l&#x27;acqua della doccia che fai
Le lenzuola del letto dove dormirai
L&#x27;hamburger di sabato sera che mangerai, che mangerai
Vorrei essere il motore della tua macchina
Cos&#xEC; di colpo mi accenderai

Tu, tu non mi basti mai
Davvero non mi basti mai
Tu, tu dolce terra mia
Dove non sono stato mai

Debbo parlarti come non faccio mai
Voglio sognarti come non ti sogno mai
Essere l&#x27;anello che porterai
La spiaggia dove camminerai
Lo specchio che ti guarda se lo guarderai, lo guarderai

Vorrei essere l&#x27;uccello che accarezzerai
E dalle tue mani non volerei mai
Vorrei esser la tomba quando morirai
E dove abiterai

Il cielo sotto il quale dormirai
Cos&#xEC; non ci lasceremo mai
Neanche se muoio e lo sai

Tu, tu non mi basti mai
Davvero non mi basti mai
Io, io, io ci provo sai
Non mi dimenticare mai`],[
"Argento",
`Era proprio la pi&#xF9; bella
e il ragazzo la guardava
quando usciva a prender l&#x27;acqua
lui con gli occhi le parlava,
lei cantava e non vedeva gli occhi suoi...

La fontana della piazza
lentamente si fermava
per sentire la ragazza,
la ragazza che cantava,
lui sognava di cantare come lei...

E un bel giorno prese un fiore dal giardino
coi colori della sera e del mattino,
la ragazza a quel profumo si volt&#xF2;,
e lui il cuore le tocc&#xF2;

I colori della vita
lui voleva raccontare,
dipingeva sopra i muri
le montagne il grano e il mare,
lei sentiva di veder con gli occhi suoi...

E la gente dalle case li guardava,
affacciata alle finestre si incantava,
dai balconi coi gerani e con le rose
sorridevano le spose.

Era proprio la pi&#xF9; bella,
e il suo sposo la guardava,
con il suo vestito bianco
che la notte rischiarava,
si dischiusero le labbra e lui cant&#xF2;...

Al sentire quella voce,
la sua sposa l&#x27;abbracciava,
e la luce della luna il suo viso le svelava,
lui cantava e lei stupita lo guard&#xF2;...

E fu festa, festa grande nel paese,
con la banda e le campane delle chiese,
il profumo del Destino ci sfior&#xF2;,
ed il cuore ci tocc&#xF2;...`
],["Alecrim",
`Alecrim alecrim dourado
Que nasceu no campo sem ser semeado
Alecrim alecrim dourado
Que nasceu no campo sem ser semeado

Foi meu amor que me disse assim
Que a flor do campo era &#xE9; o alecrim
Foi meu amor que me disse assim
Que a flor do campo &#xE9; o alecrim`],[
"Luntane, cchi&#xF9; luntane",
`Pe cant&#xE0;  sta chiarit&#xE0;
ncore me sente trem&#xE0;!
Tutte stu ciele stellate,
tutte stu mare che me fa sugn&#xE0; .
Ma pe &#x2018;tte sole, pe &#x2018;tte
esce dall&#x2019;anima me,
mezz&#x2019;a stu ciele, stu mare,
nu cantemente che nze po ten&#xE9;!

Luntane, cchi&#xF9; luntane
de li luntane stelle,
luce la luce cchi&#xF9; belle
che me fa ncore cant&#xE0;.

Marin&#xE0; , s&#x2019;ha da vug&#xE0;
tra tutta sta chiarit&#xE0;,
cante la vele a lu vente
nu cante granne che luntane v&#xE0;:
tu la si ddove vo &#xEC;
st&#x2019;aneme pe&#x2019; ne&#x2019; mur&#xEC;,
bella paranze. Luntane
&#x2018;nghe sti suspire tu i&#x2019; da men&#xEC;.

Luntane, cchi&#xF9; luntane
de li luntane stelle,
luce la luce cchi&#xF9; belle
che me fa ncore cant&#xE0;.`
]
]